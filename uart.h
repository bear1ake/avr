#include <avr/io.h>
#include <util/delay.h>
#include <stdio.h>

/*
����������� ��������� ������ ��� ������� �����
AVR/GNU Linker -> Miscellaneous -> Other Linker Flags: -Wl,-u,vfprintf -lprintf_flt
*/

static int wu_write(char c, FILE *stream);

static FILE wu_out = FDEV_SETUP_STREAM(wu_write, NULL, _FDEV_SETUP_WRITE);

static int wu_write(char c, FILE *stream)
{
	while(!(UCSR0A & (1<<UDRE)));
	UDR0 = c;
	return 0;
}

void wu_init()
{
	stdout = &wu_out;
	UCSR0A = 0x00;
	UCSR0B = (1<<TXEN);
	UCSR0C = (1<<UCSZ01) | (1<<UCSZ00);
	UBRR0H = 0;
	UBRR0L = 12;
}
﻿#include <avr/io.h>

void wa_init()
{
	ADMUX = (1<<REFS0);
	ADCSRA = (1<<ADEN) | (1<<ADSC) | (1<<ADPS2) | (1<<ADPS1);
}

int wa_measure()
{
	unsigned int adc_res;
	ADCSRA |= (1<<ADSC);
	_delay_ms(1);
	adc_res = ADCL;
	adc_res += (ADCH<<8);
	ADCSRA &= ~(1<<ADSC);
	return adc_res;
}
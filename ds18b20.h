﻿#include "w1.h"

struct DS_SPD	
{
	unsigned char MSB;
	unsigned char LSB;
	unsigned char TH;
	unsigned char TL;
	unsigned char CFG;
	unsigned char R0;
	unsigned char R1;
	unsigned char R2;
	unsigned char CRC;
}DS[w1_max_devices];
	
void ds_readSPD(int id)
{
		DS[id].LSB = w1_read_byte();
		DS[id].MSB = w1_read_byte();
		DS[id].TH = w1_read_byte();
		DS[id].TL = w1_read_byte();
		DS[id].CFG = w1_read_byte();
		DS[id].R0 = w1_read_byte();
		DS[id].R1 = w1_read_byte();
		DS[id].R2 = w1_read_byte();
		DS[id].CRC = w1_read_byte();
}

void ds_temp(unsigned char max)
{
	w1_reset();
	w1_write_byte(0xCC);
	w1_write_byte(0x44);
	_delay_ms(900);
	for (int i=0; i<max; i++)
	{
		w1_reset();
		w1_MatchROM(w1_Devices_ID[i]);
		w1_write_byte(0xBE);
		ds_readSPD(i);
	}
};

void ds_temp_dbg()
{
	w1_reset();
	w1_write_byte(0xCC);
	w1_write_byte(0x44);
	_delay_ms(900);
	w1_reset();
	w1_write_byte(0xCC);
	w1_write_byte(0xBE);	
	ds_readSPD(0);
}
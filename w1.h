﻿//					Базовые настройки 1-wire
#define w1_ddr				DDRB	// Настройка порта с 1-wire
#define w1_port				PORTB	// Сам порт с 1-wire
#define w1_pin				PINB	// Пин с 1-wire
#define w1_pbit				7	// Номер ножки в порте 1-wire
#define w1_max_devices			10	// Максимальное количество устройств на линии
//#define w1_OVERDRIVE			1	// Расскоментировать для режима OVERDRIVE

/*********************************************************************
*	Библиотека интерфейса 1-Wire
*	Даты:				30.12.2013  (Провал на поиске)
*					02.01.2013  (Рабочая версия)
*					04.03.2016  (Комментирвание)
*					21.06.2016  (Исправление)
*	Автор:				bear1ake
*	TODO:				Поддержка OVERDRIVE
**********************************************************************/

/*						Управление шиной									*/
#define w1_bit				w1_pin & (1<<w1_pbit)
#define w1_hi				w1_ddr &= ~(1<<w1_pbit); w1_port |=  (1<<w1_pbit)
#define w1_low				w1_ddr |=  (1<<w1_pbit); w1_port &= ~(1<<w1_pbit)
#define w1_free				w1_ddr &= ~(1<<w1_pbit); w1_port &= ~(1<<w1_pbit)

/*						Команды 1-wire										*/
#define w1_searchROM_cmd	0xF0
#define w1_readROM_cmd		0x33
#define w1_matchROM_cmd		0x55
#define w1_skipROM_cmd		0xCC

/*						Статусы 1-wire										*/
#define	w1_first_search		0xFF		
#define	w1_presence_err		0xFF
#define	w1_data_err			0xFE
#define w1_last_device		0x00	

/*					Стандартные задержки 1-wire								*/
//#ifndef w1_OVERDRIVE 
#define	w1_A				6
#define w1_B				64
#define w1_C				60
#define w1_D				10
#define w1_E				9
#define w1_F				55
#define w1_G				0
#define w1_H				480
#define w1_I				70
#define w1_J				410
/*				Задержки 1-wire	в режиме OVERDRIVE							
#elif
#define	w1_A				1
#define w1_B				7.5
#define w1_C				7.5
#define w1_D				2.5
#define w1_E				1
#define w1_F				2.5
#define w1_G				0
#define w1_H				70
#define w1_I				8.5
#define w1_J				40
#endif */

/*			Библиотеки ввода/вывода МК и задержки							*/
#include <avr/io.h>
#include <util/delay.h>

// Перезагрузка шины
char w1_reset()
{
	unsigned char b;
	w1_low;
	_delay_us(w1_H);
	w1_free;
	_delay_us(w1_I);
	b = w1_bit;
	_delay_us(w1_J);
	return !b;
}

// Чтение бита
unsigned char w1_read_bit()
{
	unsigned char b;
	w1_low;
	_delay_us(w1_A);
	w1_hi;
	_delay_us(w1_E);
	b = w1_bit;
	_delay_us(w1_F);
	if(b)
		return (1<<7);
	else
		return 0;
}

// Запись бита
void w1_write_bit(unsigned char b)
{
	w1_low;
	_delay_us(w1_A);
	if(b) 
	{ 
		w1_hi; 
	} 
	else 
	{ 
		w1_low; 
	};
	_delay_us(w1_B);
	w1_free;
}

// Чтение байта
unsigned char w1_read_byte()
{
	volatile unsigned char b=0, c=0;
	for(int i=0; i<8; i++)
	{
		c >>= 1;
		b = w1_read_bit();
		c |= (b & (1<<7));	
	};
	return c;
}

//Запись байта
void w1_write_byte(unsigned char b)
{
	for(int i=0; i<8; i++)
	{
		w1_write_bit(b & 0x01);
		b >>= 1;
	};
}

// Вычисление адреса
unsigned char w1_SearchROM(unsigned char diff, unsigned char *id)
{
	unsigned char i, j, next_diff;
	unsigned char b;

	if(!w1_reset())
		return w1_presence_err;													// Ошибка, нет устройств

	w1_write_byte(w1_searchROM_cmd);											// Отправляем команду поиска адреса
	next_diff = w1_last_device;													// И оставляем адрес последнего устройства без изменения
	
	i = 64;																		// 8 байт
	do																			// Делай... 1
	{
		j = 8;																	// 8 бит
		do																		// Делай... 2
		{
			b = w1_read_bit();													// Читаем первый бит
			if(w1_read_bit())													// Читаем дополнительный бит
			{																	
				if(b)															// 11 - Если есть первый и доп. биты
					return w1_data_err;											// Ошибка данных
			}
			else
				if(!b)															// 00 = 2 устройства														
					if( diff > i || ( (*id & 1) && diff != i ) ) 
					{
						b = 1;													// Теперь одно
						next_diff = i;											// Следующее устройство
					}
			w1_write_bit(b);													// Записываем бит
			*id >>= 1;
			if(b)
				*id |= 0x80;													// Сохраняем бит
			i--;																// Следующий бит
		}
		while(--j);																// ... 2 Пока есть биты
		id++;																	// Следующий байт
	}while(i);																	// ... 1 Пока есть байты
	return next_diff;															// Возращаем для следующего поиска
}

// Найти адрес устройства
void w1_FindROM(unsigned char *diff, unsigned char id[])
{
	while(1)
	{
		// Присваиваем адрес в матрицу
		*diff = w1_SearchROM(*diff, &id[0]);
		// Если все очень плохо, то прекращаем
		if (*diff==w1_presence_err || *diff==w1_data_err || *diff == w1_last_device ) 
			return;
		// Если все ОК, то выходим
		return;
	}
}

// Чтение адреса одного устройства
unsigned char w1_ReadROM(unsigned char *buffer)
{
	if (!w1_reset()) 
		return 0;
	w1_write_byte(w1_readROM_cmd);
	for (unsigned char i=0; i<8; i++)
		buffer[i] = w1_read_byte();
	return 1;
}

// Обращение по адресу
unsigned char w1_MatchROM(unsigned char *rom)
{
	if (!w1_reset()) 
		return 0;
	w1_write_byte(w1_matchROM_cmd);
	for(unsigned char i=0; i<8; i++)
		w1_write_byte(rom[i]);
	return 1;
}

// Массив адресов устройств
unsigned char w1_Devices_ID[w1_max_devices][8];

// Поиск всех устройств на шине
unsigned char w1_search() 
{
	// Переменные для работы
	unsigned char	i;									
	unsigned char	id[8];
	unsigned char	diff, sensors_count;

	// Ноль устройств
	sensors_count = 0;	

	// Начиная с первого устройства, заканчивая последним по поиску или количеству допустимых устройств
	for(diff = w1_first_search; diff != w1_last_device && sensors_count < w1_max_devices; )
	{
		w1_FindROM(&diff, &id[0]);				// Записываем адрес						
		if(diff == w1_presence_err)				// Если нет устройств,
			break;								// то прекращаем
		if(diff == w1_data_err)					// Если ошибка данных,
			break;								// то прекращаем

		for(i=0;i<8;i++)						// Кладем найденный адрес в матрицу
			w1_Devices_ID[sensors_count][i] = id[i];
		
		sensors_count++;						// Следующее устройств!
	}
	return sensors_count;						// Возвращаем количество устройств
}

// Инициализация
unsigned char w1_init()
{
	w1_reset();											// На старт!
	unsigned char w1_max_dev_count = w1_search();		// Внимание...
	return w1_max_dev_count;							// МАРШ! (Возврат найденных устройств)
} 
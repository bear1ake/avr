﻿/*
	Высотомер Junior Skills
	Автор: bear1ake
	Настройки:
	AVR/GNU Linker -> Miscellaneous -> Other Linker Flags: -Wl,-u,vfprintf -lprintf_flt
	Доп. подключение:
	PA0[J3(6)] -> Светодиод -> GND[J3(2)]
	PA1[J3(8)] -> Светодиод -> GND[J3(4)]
*/


#define F_CPU 8000000

#include <avr/io.h>
#include <stdio.h>
#include <util/delay.h>
#include <math.h>

#include "ds18b20.h"
#include "adc.h"
#include "uart.h"

void radio_init()
{
	wu_init();
	DDRE = 0b00111100;
	PORTE = (1<<PE2) | (1<<PE3) | (1<<PE4);
	
	_delay_ms(100);
	
	printf("%c%c%c%c%c%c%c%c%c%c%c%c", 0x7E, 0x7E, 0x7E, 0x7E, 0x7E, 0x7E, 0x7E, 0x7E, 0x6B, 0, 0x1E, 0x1E);
	
	_delay_ms(100);
	
	PORTE |= (1<<PE4);
}

double mpx4115_press()
{
	double Vout, Vcc=5.0, P;			
	int adc;							
	adc = wa_measure();					
	Vout = adc * (5.0/1023.0);			
	P = ((Vout/Vcc)+0.095)/0.009;		
	return P;							
}										
double P0_init()
{
	double p0=0, P;
	for(int i=0; i<10; i++)
		mpx4115_press();
	for(int i=0; i<10; i++)
		p0 += mpx4115_press();
	P = p0 / 10.0;
	return P;
}

int check()
{
	int tcheck=0, acheck=0;
	int press;
	tcheck = w1_init();
	press = wa_measure();
	if(press>300 && press<1000)
		acheck = 1;
	if(tcheck)
		PORTA |= (1<<PA0);
	if(acheck)
		PORTA |= (1<<PA1);
	if(tcheck && acheck)
		return 1;
	else
		return 0;
}

int main()
{
	DDRG = (1<<PG3);
	PORTG = (1<<PG3);
	DDRA = (1<<PA0) | (1<<PA1);
	PORTA = 0;
	DDRC = 0;
	PORTC = 0;
	
	double Temp, P, P0, DH;
	int U = w1_init();	
	wa_init();
	radio_init();
	P0 = P0_init();
	
	for(int i=0; i<U; i++)
	{
		for(int j=0; j<8; j++)
			printf("%X ", w1_Devices_ID[i][j]);
		printf("\r\n");
	}
	
	PORTG = 0;
	for(int i=0; i<3; i++)
	{
		PORTG = (1<<PG3);
		_delay_ms(300);
		PORTA = (1<<PA0);
		_delay_ms(300);
		PORTA |= (1<<PA1);
		_delay_ms(300);
		PORTG = 0;
		PORTA = 0;
		_delay_ms(300);
	}
	int C = check();
	PORTG = C ? (1<<PG3) : 0;
	
	printf("\r\nP0 = %f\r\n", P0);
	
	while(1)
	{
		ds_temp(U);
		Temp = (DS[0].MSB*0x100+DS[0].LSB) / 16.0;
		P = mpx4115_press();
		printf("C - %d | ", wa_measure());
		DH = 18400*(1+0.003665*Temp)*log10(P0/P);
		printf("P = %f | T = %f | H = %f | C = %d\r\n", P, Temp, DH, C);
	}
}